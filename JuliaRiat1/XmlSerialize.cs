﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace JuliaRiat1
{

    public class XmlSerialize : ISerializer
    {
        private Dictionary<Type,XmlSerializer> serializers = new Dictionary<Type, XmlSerializer>();
        private object GetSerializer(Type key)
        {
            if (serializers.ContainsKey(key))
            {
                return (object)serializers[key];
            }
            else
            {
                serializers.Add(key, new XmlSerializer(key));
                return (object)serializers[key];
            }
        }
        private XmlWriterSettings settings = new XmlWriterSettings() { OmitXmlDeclaration = true };
        private XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();

        public XmlSerialize()
        {

            namespaces.Add("", "");
           
        }
        T ISerializer.Deserialize<T>(string serializedData)
        {
            var serializer = new XmlSerializer(typeof(T));
            var stream = new MemoryStream() { Position = 0 } ;
            using (var sw = new StreamWriter(stream))
            {
                sw.WriteLine(serializedData);
                sw.Flush();
                stream.Position = 0;
                var reader = XmlReader.Create(stream);
                return (T)serializer.Deserialize(reader);
            }
        }
        string ISerializer.Serialize<T>(T data)
        {            
            var serializer = new XmlSerializer(typeof(T));
            var stream = new MemoryStream();
            namespaces.Add("", "");
            var writer = XmlWriter.Create(stream, settings);
            serializer.Serialize(writer, data, namespaces);
            using (var sr = new StreamReader(stream))
            {
                stream.Position = 0;
                return sr.ReadToEnd();
            }
        }
    }
}
