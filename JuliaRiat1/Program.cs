﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuliaRiat1
{
    class Program
    {
        static string GetAnswer<T>(T serializer, string serializedInput)
        {
            var iSerializer = (ISerializer)serializer;
            var input = iSerializer.Deserialize<Input>(serializedInput);
            var output = Converter.ConvertToOutput(input);
            return iSerializer.Serialize<Output>(output);
        }

        static void Main(string[] args)
        {
            var serializingType = Console.ReadLine();
            var serializedInput = Console.ReadLine();          
            var serializedOutput = String.Empty;
            switch (serializingType)
            {
                case "Json":
                    serializedOutput = GetAnswer<JSONSerialize>(new JSONSerialize(), serializedInput);
                    break;
                case "Xml":
                    serializedOutput = GetAnswer<XmlSerialize>(new XmlSerialize(), serializedInput);
                    break;
                default: throw new Exception("Неизвестный тип сериализации");
            }
            Console.WriteLine(serializedOutput);
        
        }
    }
}
