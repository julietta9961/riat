﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JuliaRiat1
{
    class JSONSerialize : ISerializer
    {
        T ISerializer.Deserialize<T>(string serializedData)
        {
            return JsonConvert.DeserializeObject<T>(serializedData);
        }

        string ISerializer.Serialize<T>(T data)
        {
            return JsonConvert.SerializeObject(data);
        }
    }
}
